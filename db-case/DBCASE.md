# devops

### Database - Case 1

Couchbase için gereken kurulum işlemleri yapıldıktan sonra <host_ip>:8091 ip adresine bağlanılır. Benim durumumda bu ip: 192.168.1.37:8091

Daha sonra aşağıdaki işlemlerimi yaparak bir cluster kurdum.

![1.1.1](/img/1.1.1.png)

![1.1.2](/img/1.1.2.png)

![1.1.3](/img/1.1.3.png)

![1.1.4](/img/1.1.4.png)

Bu işlemden sonra node eklemek için docker servisini kurdum. Ardından docker üzerinden db oluşturdum. Bunun için:

docker run -d -p 7091:8091--name test1 couchbase/server

Daha sonra web browser üzerinden 192.168.1.91:7091 ip adresine bağlanıp bu sefer Join Existing Cluster seçeneği ile node ekledik. En son olarak rebalance işlemi yapıldı.

Not: Case 2'yi Docker'da aldığım bir hata nedeniyle yapamadım.


